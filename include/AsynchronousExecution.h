/*
 * AsynchronousExecution.h
 *
 * Copyright 2016 Remi BERTHO <remi.bertho@openmailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program  is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef ASYNCHRONOUS_EXECUTION_H_INCLUDED
#define ASYNCHRONOUS_EXECUTION_H_INCLUDED

#include <thread>
#include <functional>
#include <exception>
#include <memory>
#include <stdexcept>

namespace AsynchronousExecution
{

	namespace
	{
		/**
		 * @brief Thread function that exectute the function
		 * @param execution_function	The function to execute
		 * @param return_function		The callback function to execute when the function is done with the return value, can be nullptr
		 * @param exception_function	The callback function to execute if an exception occured, can be nullptr
		 * @param lock_function		The lock function if needed, can be nullptr
		 * @param unlock_function		The unlock function if needed, can be nullptr
		 * @param object_alive			A object in a shared ponter thant need to be alive during the function, can be nullptr
		 *
		 * @tparam ReturnType			The return type of the execution_function
		 * @tparam ExceptionType		The exception type that can occured during the execution_function
		 * @tparam ObjectType			The type of object_alive
		 */
		template <typename ReturnType, typename ExceptionType, typename ObjectType>
		static void execThread(std::function<ReturnType()> execution_function,
				std::function<void(ReturnType&)>		   return_function,
				std::function<void(ExceptionType&)>		   exception_function,
				std::function<void(void)>				   lock_function,
				std::function<void(void)>				   unlock_function,
				std::shared_ptr<ObjectType>				   object_alive)
		{
			try
			{
				if (lock_function != nullptr)
					lock_function();

				ReturnType ret = std::invoke(execution_function);

				if (unlock_function != nullptr)
					unlock_function();

				if (return_function != nullptr)
					return_function(ret);
			}
			catch (ExceptionType& ex)
			{
				if (unlock_function != nullptr)
					unlock_function();

				if (exception_function != nullptr)
					exception_function(ex);
			}
		}


		/**
		 * @brief Thread function that exectute the function, void version
		 * @param execution_function	The function to execute
		 * @param exception_function	The callback function to execute if an exception occured, can be nullptr
		 * @param return_function		The callback function to execute when the function is done, can be nullptr
		 * @param lock_function		The lock function if needed, can be nullptr
		 * @param unlock_function		The unlock function if needed, can be nullptr
		 * @param object_alive			A object in a shared ponter thant need to be alive during the function, can be nullptr
		 *
		 * @tparam ExceptionType		The exception type that can occured during the execution_function
		 * @tparam ObjectType			The type of object_alive
		 */
		template <typename ExceptionType, typename ObjectType>
		static void execVoidThread(std::function<void()> execution_function,
				std::function<void(void)>				 return_function,
				std::function<void(ExceptionType&)>		 exception_function,
				std::function<void(void)>				 lock_function,
				std::function<void(void)>				 unlock_function,
				std::shared_ptr<ObjectType>				 object_alive)
		{
			try
			{
				if (lock_function != nullptr)
					lock_function();

				std::invoke(execution_function);

				if (unlock_function != nullptr)
					unlock_function();

				if (return_function != nullptr)
					return_function();
			}
			catch (ExceptionType& ex)
			{
				if (unlock_function != nullptr)
					unlock_function();

				if (exception_function != nullptr)
					exception_function(ex);
			}
		}
	}	 // namespace

	/**
	 * @brief Launch a function asynchronously with possible callback and lock
	 * @param execution_function	The function to execute
	 * @param return_function		The callback function to execute after with the return value, can be nullptr
	 * @param exception_function	The callback function to execute if an exception occured, can be nullptr
	 * @param lock_function		The lock function if needed, can be nullptr
	 * @param unlock_function		The unlock function if needed, can be nullptr
	 * @param object_alive			A object in a shared pointer thant need to be alive during the function, can be nullptr.
	 *
	 * @tparam ReturnType			The return type of the execution_function
	 * @tparam ExceptionType		The exception type that can occured during the execution_function
	 * @tparam ObjectType			The type of object_alive
	 */
	template <typename ReturnType, typename ExceptionType = std::exception, typename ObjectType = int>
	void execFunction(std::function<ReturnType()> execution_function,
			std::function<void(ReturnType&)>	  return_function	 = nullptr,
			std::function<void(ExceptionType&)>	  exception_function = nullptr,
			std::function<void(void)>			  lock_function		 = nullptr,
			std::function<void(void)>			  unlock_function	 = nullptr,
			std::shared_ptr<ObjectType>			  object_alive		 = nullptr)
	{
		if ((lock_function == nullptr && unlock_function != nullptr) || (unlock_function == nullptr && lock_function != nullptr))
			throw std::invalid_argument("Use of a lock function without an unlock function.");

		std::thread th(execThread<ReturnType, ExceptionType, ObjectType>,
				execution_function,
				return_function,
				exception_function,
				lock_function,
				unlock_function,
				object_alive);
		th.detach();
	}


	/**
	 * @brief Launch a function asynchronously with possible callback and lock, void version
	 * @param execution_function	The function to execute
	 * @param exception_function	The callback function to execute if an exception occured, can be nullptr
	 * @param return_function		The callback function to execute when the function is done, can be nullptr
	 * @param lock_function		The lock function if needed, can be nullptr
	 * @param unlock_function		The unlock function if needed, can be nullptr
	 * @param object_alive			A object in a shared pointer thant need to be alive during the function, can be nullptr.
	 *
	 * @tparam ExceptionType		The exception type that can occured during the execution_function
	 * @tparam ObjectType			The type of object_alive
	 */
	template <typename ExceptionType = std::exception, typename ObjectType = int>
	void execVoidFunction(std::function<void()> execution_function,
			std::function<void(void)>			return_function	   = nullptr,
			std::function<void(ExceptionType&)> exception_function = nullptr,
			std::function<void(void)>			lock_function	   = nullptr,
			std::function<void(void)>			unlock_function	   = nullptr,
			std::shared_ptr<ObjectType>			object_alive	   = nullptr)
	{
		if ((lock_function == nullptr && unlock_function != nullptr) || (unlock_function == nullptr && lock_function != nullptr))
			throw std::invalid_argument("Use of a lock function without an unlock function.");

		std::thread th(execVoidThread<ExceptionType, ObjectType>,
				execution_function,
				return_function,
				exception_function,
				lock_function,
				unlock_function,
				object_alive);
		th.detach();
	}
};	  // namespace AsynchronousExecution

#endif	  // ASYNCHRONOUS_EXECUTION_H_INCLUDED
