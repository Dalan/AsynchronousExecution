/*
 * lockable.h
 *
 * Copyright 2016 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program  is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef LOCKABLE_H_INCLUDED
#define LOCKABLE_H_INCLUDED

#include <functional>
#include <memory>
#include <thread>

#include "AsynchronousExecution.h"

namespace AsynchronousExecution
{
	/**
	 * @class _LockGuard
	 * @author Rémi BERTHO
	 * @date 24/08/2016
	 * @file lockable.h
	 * @brief A generic lock guard
	 */
	class _LockGuard
	{
	};

	/** LockGuard. The lock is locked as long as the object is available */
	typedef std::shared_ptr<_LockGuard> LockGuard;

	/**
	 * @class Lockable
	 * @author Rémi BERTHO
	 * @date 27/07/2016
	 * @file lockable.h
	 * @brief A class that own a lock and can execute asynchronous functions
	 */
	class Lockable
	{
	public:
		/**
		 * @brief  Default constructor
		 */
		Lockable();

		/**
		 * @brief Destructor
		 */
		virtual ~Lockable();

		/**
		 * @brief  Copy constructor
		 */
		Lockable(const Lockable& l);

		/*!
		 *  @brief Operator =
		 *  @param l another Lockable
		 */
		Lockable& operator=(const Lockable& l);

		/**
		 * @brief Lock the object. If the object is locked, no one else can lock
		 */
		virtual void lock() const = 0;

		/**
		 * @brief Unlock the object
		 */
		virtual void unlock() const = 0;

		/**
		 * @brief Get a lock. The object is locked as long as the Lock exist.
		 * @return the Lock
		 */
		virtual LockGuard getLock() const = 0;

		/**
		 * @brief Get a function that lock the object
		 * @return the function
		 */
		std::function<void(void)> getLockFuction() const;

		/**
		 * @brief Get a function that unlock the object
		 * @return the function
		 */
		std::function<void(void)> getUnlockFuction() const;

		/**
		 * @brief Get a C++11 function to be passed to execAsynchronously from a subclass function
		 * @param function		the function to get (eg &Lockable::lock)
		 * @param args			the arguments to be passed to the function
		 * @return 				the C++11 function
		 *
		 * @tparam ClassType		The subclass type
		 * @tparam ReturnType		The return type of the function
		 * @tparam FunctionType	The function type, is usually deduce by the compiler
		 * @tparam Args			The arguments's type of args
		 */
		template <typename ClassType, typename ReturnType, typename FunctionType, typename... Args>
		std::function<ReturnType()> getFunction(FunctionType function, const Args&... args)
		{
			return [=]() -> ReturnType { return static_cast<ReturnType>((static_cast<ClassType*>(this)->*function)(args...)); };
		}

		template <typename ClassType, typename ReturnType, typename FunctionType, typename... Args>
		std::function<ReturnType()> getFunction(FunctionType function, const Args&... args) const
		{
			return [=]() -> ReturnType { return static_cast<ReturnType>((static_cast<ClassType*>(this)->*function)(args...)); };
		}

		/**
		 * @brief Get a C++11 function to be passed to execAsynchronously from a void subclass function, void version
		 * @param function		the function to get (eg &Lockable::lock)
		 * @param args			the arguments to be passed to the function
		 * @return 				the C++11 function
		 *
		 * @tparam ClassType		The subclass type
		 * @tparam FunctionType	The function type, is usually deduce by the compiler
		 * @tparam Args			The arguments's type of args
		 */
		template <typename ClassType, typename FunctionType, typename... Args>
		std::function<void()> getVoidFunction(FunctionType function, const Args&... args)
		{
			return [=]() -> void { (static_cast<ClassType*>(this)->*function)(args...); };
		}

		template <typename ClassType, typename FunctionType, typename... Args>
		std::function<void()> getVoidFunction(FunctionType function, const Args&... args) const
		{
			return [=]() -> void { (static_cast<ClassType*>(this)->*function)(args...); };
		}

		/**
		 * @brief Launch a function asynchronously with possible callback and lock
		 * @param function			The function to execute (eg &Lockable::lock)
		 * @param return_function		The callback function to execute after with the return value, can be nullptr
		 * @param exception_function	The callback function to execute if an exception occured, can be nullptr
		 * @param args				the arguments to be passed to the function
		 * @param object_alive			A object in a shared pointer thant need to be alive during the function, can be nullptr. It is this wrapped
		 * in a shared_ptr. It can be get from std::enable_shared_from_this.
		 *
		 * @tparam ReturnType			The return type of the execution_function
		 * @tparam ExceptionType		The exception type that can occured during the execution_function
		 * @tparam ClassType			The subclass type
		 * @tparam FunctionType		The function type, is usually deduce by the compiler
		 * @tparam Args				The arguments's type of args
		 */
		template <typename ReturnType, typename ExceptionType = std::exception, typename ClassType, typename FunctionType, typename... Args>
		void execAsynchronously(std::function<void(ReturnType&)> return_function,
				std::function<void(ExceptionType&)>				 exception_function,
				std::shared_ptr<ClassType>						 object_alive,
				FunctionType									 function,
				const Args&... args)
		{
			execFunction<ReturnType, ExceptionType>(getFunction<ClassType, ReturnType>(function, args...),
					return_function,
					exception_function,
					getLockFuction(),
					getUnlockFuction(),
					object_alive);
		}

		template <typename ReturnType, typename ExceptionType = std::exception, typename ClassType, typename FunctionType, typename... Args>
		void execAsynchronously(std::function<void(ReturnType&)> return_function,
				std::function<void(ExceptionType&)>				 exception_function,
				std::shared_ptr<ClassType>						 object_alive,
				FunctionType									 function,
				const Args&... args) const
		{
			execFunction<ReturnType, ExceptionType>(getFunction<ClassType, ReturnType>(function, args...),
					return_function,
					exception_function,
					getLockFuction(),
					getUnlockFuction(),
					object_alive);
		}

		/**
		 * @brief Launch a function asynchronously with possible callback and without lock
		 * @param function			The function to execute (eg &Lockable::lock)
		 * @param return_function		The callback function to execute after with the return value, can be nullptr occured, can be nullptr
		 * @param args				the arguments to be passed to the function
		 * @param object_alive			A object in a shared pointer thant need to be alive during the function, can be nullptr. It is this wrapped
		 * in a shared_ptr. It can be get from std::enable_shared_from_this.
		 *
		 * @tparam ReturnType			The return type of the execution_function
		 * @tparam ExceptionType		The exception type that can occured during the execution_function
		 * @tparam ClassType			The subclass type
		 * @tparam FunctionType		The function type, is usually deduce by the compiler
		 * @tparam Args				The arguments's type of args
		 */
		template <typename ReturnType, typename ExceptionType = std::exception, typename ClassType, typename FunctionType, typename... Args>
		void execAsynchronouslyWithoutLock(std::function<void(ReturnType&)> return_function,
				std::function<void(ExceptionType&)>							exception_function,
				std::shared_ptr<ClassType>									object_alive,
				FunctionType												function,
				const Args&... args)
		{
			execFunction<ReturnType, ExceptionType>(getFunction<ClassType, ReturnType>(function, args...),
					return_function,
					exception_function,
					nullptr,
					nullptr,
					object_alive);
		}

		template <typename ReturnType, typename ExceptionType = std::exception, typename ClassType, typename FunctionType, typename... Args>
		void execAsynchronouslyWithoutLock(std::function<void(ReturnType&)> return_function,
				std::function<void(ExceptionType&)>							exception_function,
				std::shared_ptr<ClassType>									object_alive,
				FunctionType												function,
				const Args&... args) const
		{
			execFunction<ReturnType, ExceptionType>(getFunction<ClassType, ReturnType>(function, args...),
					return_function,
					exception_function,
					nullptr,
					nullptr,
					object_alive);
		}

		/**
		 * @brief Launch a function asynchronously with possible callback and lock, void version
		 * @param function			The function to execute (eg &Lockable::lock)
		 * @param exception_function	The callback function to execute if an exception occured, can be nullptr
		 * @param return_function		The callback function to execute when the function is done, can be nullptr
		 * @param args				the arguments to be passed to the function
		 * @param object_alive			A object in a shared pointer thant need to be alive during the function, can be nullptr. It is this wrapped
		 * in a shared_ptr. It can be get from std::enable_shared_from_this.
		 *
		 * @tparam ExceptionType		The exception type that can occured during the execution_function
		 * @tparam ClassType			The subclass type
		 * @tparam FunctionType		The function type, is usually deduce by the compiler
		 * @tparam Args				The arguments's type of args
		 */
		template <typename ExceptionType = std::exception, typename ClassType, typename FunctionType, typename... Args>
		void execVoidAsynchronously(std::function<void(void)> return_function,
				std::function<void(ExceptionType&)>			  exception_function,
				std::shared_ptr<ClassType>					  object_alive,
				FunctionType								  function,
				const Args&... args)
		{
			execVoidFunction<ExceptionType>(getVoidFunction<ClassType>(function, args...),
					return_function,
					exception_function,
					getLockFuction(),
					getUnlockFuction(),
					object_alive);
		}
		template <typename ExceptionType = std::exception, typename ClassType, typename FunctionType, typename... Args>
		void execVoidAsynchronously(std::function<void(void)> return_function,
				std::function<void(ExceptionType&)>			  exception_function,
				std::shared_ptr<ClassType>					  object_alive,
				FunctionType								  function,
				const Args&... args) const
		{
			execVoidFunction<ExceptionType>(getVoidFunction<ClassType>(function, args...),
					return_function,
					exception_function,
					getLockFuction(),
					getUnlockFuction(),
					object_alive);
		}


		/**
		 * @brief Launch a function asynchronously with possible callback and without lock, void version
		 * @param function			The function to execute (eg &Lockable::lock)
		 * @param exception_function	The callback function to execute if an exception occured, can be nullptr
		 * @param return_function		The callback function to execute when the function is done, can be nullptr
		 * @param args				the arguments to be passed to the function
		 * @param object_alive			A object in a shared pointer thant need to be alive during the function, can be nullptr. It is this wrapped
		 * in a shared_ptr. It can be get from std::enable_shared_from_this.
		 *
		 * @tparam ExceptionType		The exception type that can occured during the execution_function
		 * @tparam ClassType			The subclass type
		 * @tparam FunctionType		The function type, is usually deduce by the compiler
		 * @tparam Args				The arguments's type of args
		 */
		template <typename ExceptionType = std::exception, typename ClassType, typename FunctionType, typename... Args>
		void execVoidAsynchronouslyWithoutLock(std::function<void(void)> return_function,
				std::function<void(ExceptionType&)>						 exception_function,
				std::shared_ptr<ClassType>								 object_alive,
				FunctionType											 function,
				const Args&... args)
		{
			execVoidFunction<ExceptionType>(
					getVoidFunction<ClassType>(function, args...), return_function, exception_function, nullptr, nullptr, object_alive);
		}

		template <typename ExceptionType = std::exception, typename ClassType, typename FunctionType, typename... Args>
		void execVoidAsynchronouslyWithoutLock(std::function<void(void)> return_function,
				std::function<void(ExceptionType&)>						 exception_function,
				std::shared_ptr<ClassType>								 object_alive,
				FunctionType											 function,
				const Args&... args) const
		{
			execVoidFunction<ExceptionType>(
					getVoidFunction<ClassType>(function, args...), return_function, exception_function, nullptr, nullptr, object_alive);
		}
	};

}	// namespace AsynchronousExecution

#endif	// LOCKABLE_H_INCLUDED
