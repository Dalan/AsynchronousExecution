/*
* mutex_lockable.h
*
* Copyright 2016 Remi BERTHO <remi.bertho@dalan.fr>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* This program  is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/

#ifndef SPINLOCK_LOCKABLE_H_INCLUDED
#define SPINLOCK_LOCKABLE_H_INCLUDED

#include <atomic>

#include "lockable.h"

namespace AsynchronousExecution
{
	/** A spinlock */
	typedef std::atomic_flag Spinlock;
	
	/**
	 * @class _SpinlockGuard
	 * @author Rémi BERTHO
	 * @date 24/08/2016
	 * @file spinlock_lockable.h
	 * @brief A spinlock guard 
	 */
	class _SpinlockGuard: public _LockGuard
	{
		friend class SpinlockLockable;
		
	private:
		/** The lockable that will be locked */
		const Lockable* guard_ = nullptr;
		
		/**
		 * @brief Disconnect the lockable and release it
		 */
		void disconnect();
		
	public:
		/**
		 * @brief Default constructor
		 * @param spin the SpinlockLockable
		 */
		_SpinlockGuard(const Lockable* spin);
		
		_SpinlockGuard(const _SpinlockGuard& l) = delete;
		
		_SpinlockGuard& operator=(const _SpinlockGuard& l) = delete;
		
		/**
		 * @brief Destrucotr
		 */
		~_SpinlockGuard();
	};

	
	/**
	 * @class MutexLockable
	 * @author Rémi BERTHO
	 * @date 15/08/2016
	 * @file spinlock_lockable.h
	 * @brief A class that own a mutex and can execute asynchronous functions 
	 */
	class SpinlockLockable : public Lockable
	{
	private:
		/** The mutex to lock the object */
		mutable Spinlock lock_ = ATOMIC_FLAG_INIT;
		
		/** The guard */
		mutable _SpinlockGuard* guard_ = nullptr;
		
		
	public:
		/**
		 * @brief  Default constructor
		 */
		SpinlockLockable();
		
		/**
		 * @brief Destructor
		 */
		virtual ~SpinlockLockable();

		/**
		 * @brief  Copy constructor
		 */
		SpinlockLockable(const SpinlockLockable& l);

		/*!
		 *  @brief Operator =
		 *  @param l another MutexLockable
		 */
		SpinlockLockable& operator=(const SpinlockLockable& l);
		
		
		/**
		 * @brief Lock the object. If the object is locked, no one else can lock
		 */
		virtual void lock() const;

		/**
		 * @brief Unlock the object
		 */
		virtual void unlock() const;

		/**
		 * @brief Get a lock. The object is locked as long as the Lock exist.
		 * @return the Lock
		 */
		virtual LockGuard getLock() const;
	};
}

#endif // SPINLOCK_LOCKABLE_H_INCLUDED
