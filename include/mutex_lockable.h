/*
* mutex_lockable.h
*
* Copyright 2016 Remi BERTHO <remi.bertho@dalan.fr>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* This program  is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*
*
*/

#ifndef MUTEX_LOCKABLE_H_INCLUDED
#define MUTEX_LOCKABLE_H_INCLUDED

#include <mutex>

#include "lockable.h"

namespace AsynchronousExecution
{
	/** Use the standard mutex as Mutex */
	typedef std::mutex Mutex;
	
	/**
	 * @class _MutexGuard
	 * @author Rémi BERTHO
	 * @date 24/08/2016
	 * @file mutex_lockable.h
	 * @brief A mutex guard 
	 */
	class _MutexGuard : public _LockGuard
	{
	private:
		/** The lock guard for the mutex */
		std::lock_guard<Mutex>* guard_;
		
	public:
		/**
		 * @brief The lock guard constructor
		 * @param mut the mutex that will be owned
		 */
		_MutexGuard(Mutex& mut);
		
		_MutexGuard(const _MutexGuard& l) = delete;
		
		_MutexGuard& operator=(const _MutexGuard& l) = delete;
		
		/**
		 * @brief Destrucotr
		 */
		~_MutexGuard();
	};
	
	/**
	 * @class MutexLockable
	 * @author Rémi BERTHO
	 * @date 15/08/2016
	 * @file mutex_lockable.h
	 * @brief A class that own a mutex and can execute asynchronous functions 
	 */
	class MutexLockable : public Lockable
	{
	private:
		/** The mutex to lock the object */
		mutable Mutex mut_;
		
		
	public:
		/**
		 * @brief  Default constructor
		 */
		MutexLockable();
		
		/**
		 * @brief Destructor
		 */
		virtual ~MutexLockable();

		/**
		 * @brief  Copy constructor
		 */
		MutexLockable(const MutexLockable& l);

		/*!
		 *  @brief Operator =
		 *  @param l another MutexLockable
		 */
		MutexLockable& operator=(const MutexLockable& l);
		
		
		/**
		 * @brief Lock the object. If the object is locked, no one else can lock
		 */
		virtual void lock() const;

		/**
		 * @brief Unlock the object
		 */
		virtual void unlock() const;

		/**
		 * @brief Get a lock. The object is locked as long as the Lock exist.
		 * @return the Lock
		 */
		virtual LockGuard getLock() const;
	};
}

#endif // MUTEX_LOCKABLE_H_INCLUDED