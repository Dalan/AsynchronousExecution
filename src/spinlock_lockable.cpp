/*
 * mutex_lockable.cpp
 *
 * Copyright 2016 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program  is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#if defined __unix__ || defined __MINGW64__
#include <xmmintrin.h>
#endif

#include "spinlock_lockable.h"

namespace AsynchronousExecution {
_SpinlockGuard::_SpinlockGuard(const Lockable *spin) {
  guard_ = spin;
  guard_->lock();
}

_SpinlockGuard::~_SpinlockGuard() {
  if (guard_ != nullptr)
	guard_->unlock();
}

void _SpinlockGuard::disconnect() { guard_ = nullptr; }

SpinlockLockable::SpinlockLockable() : Lockable() {}

SpinlockLockable::SpinlockLockable(const SpinlockLockable &l) : Lockable(l) {}

SpinlockLockable &SpinlockLockable::operator=(const SpinlockLockable &l) {
  if (this == &l)
	return *this;

  return *this;
}

SpinlockLockable::~SpinlockLockable() {
  if (guard_ != nullptr)
	guard_->disconnect();
}

void SpinlockLockable::lock() const {
  while (lock_.test_and_set())
	_mm_pause();
}

void SpinlockLockable::unlock() const {
  guard_ = nullptr;
  lock_.clear();
}

LockGuard SpinlockLockable::getLock() const {
  auto ret = std::make_shared<_SpinlockGuard>(this);
  guard_ = ret.get();
  ;
  return ret;
}
} // namespace AsynchronousExecution
