/*
 * lockable.cpp
 *
 * Copyright 2016 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program  is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "lockable.h"

namespace AsynchronousExecution {

Lockable::Lockable() {}

Lockable::Lockable(const Lockable &l) {}

Lockable &Lockable::operator=(const Lockable &l) {
  if (this == &l)
	return *this;

  return *this;
}

Lockable::~Lockable() {}

std::function<void(void)> Lockable::getLockFuction() const {
  return std::bind(&Lockable::lock, this);
}

std::function<void(void)> Lockable::getUnlockFuction() const {
  return std::bind(&Lockable::unlock, this);
}

} // namespace AsynchronousExecution
