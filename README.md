Asynchronous Execution library
======

Informations
------------

An library for running functions asynchronously

If you have any remarks or question, don't hesitate to report to me.

Documentation
-------------

The documentation is made by Doxygen.

You can built it with with running `doxygen` in the main directory.

Compilation
------------

To compile csuper you need a C++17 compliant compiler. Tested with:

* GCC 8.2
* Clang 6.0
* Visual Studio 2017

It use cmake to create a static library.

In Linux you can use this:
```
git clone https://framagit.org/Dalan/AsynchronousExecution.git
cd AsynchronousExecution
mkdir build
cmake ..
make
```

If you want to build the test, add `-DTEST=ON` in the cmake command.

The compilation should work under Linux, Mac et Windows.

Additional Information
----------------------

If you have any remark, do not hesitate to open an issue.