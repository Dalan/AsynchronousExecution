/*
 * AsynchronousExecution.cpp
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program  is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "AsynchronousExecution.h"
#include <catch2/catch_all.hpp>
#include <mutex>
#include <iostream>

using namespace AsynchronousExecution;
using namespace std;

void blop([[maybe_unused]] int a)
{
}

int launchException()
{
	throw std::runtime_error("Error");
}

TEST_CASE("AsynchronousExecution", "[AsynchronousExecution]")
{
	mutex mut;
	bool  return_called	= false;
	bool  exception_called = false;

	SECTION("Exception")
	{
		execFunction<int, runtime_error>(launchException,
				[&]([[maybe_unused]] auto& ex) { return_called = true; },
				[&]([[maybe_unused]] auto& ex) { exception_called = true; },
				bind(&mutex::lock, ref(mut)),
				bind(&std::mutex::unlock, ref(mut)));
		this_thread::sleep_for(chrono::milliseconds(1));
		CHECK(return_called == false);
		CHECK(exception_called == true);
	}

	execFunction<int*>(
			[]() {
				this_thread::sleep_for(chrono::milliseconds(100));
				return new int(3);
			},
			[](auto& i) {
				cout << *i << endl;
				delete i;
			});

	execVoidFunction<>(bind(blop, 78));

	this_thread::sleep_for(chrono::milliseconds(200));
}
