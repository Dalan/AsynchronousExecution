find_package(Catch2 3 REQUIRED)

file(
	GLOB
	TEST_SOURCE_FILES
	"*.cpp"
)

add_executable(
	test_async
	${TEST_SOURCE_FILES}
)

target_link_libraries(
	test_async
	asynchronous_execution
	Catch2::Catch2WithMain
)

IF(CODE_COVERAGE MATCHES "TRUE")
	include(CodeCoverage)
	setup_target_for_coverage(testcoverage test_async coverage)
ENDIF()

include(Catch)
catch_discover_tests(test_async)

