/*
 * lockable.cpp
 *
 * Copyright 2016-2018 Remi BERTHO <remi.bertho@dalan.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program  is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "AsynchronousExecution.h"
#include "mutex_lockable.h"
#include "spinlock_lockable.h"

#include <catch2/catch_all.hpp>
#include <mutex>
#include <iostream>

using namespace AsynchronousExecution;
using namespace std;

class toto : public SpinlockLockable, public enable_shared_from_this<toto>
{
private:
	int a;

public:
	toto(int aa) : a(aa)
	{
	}

	int tata(int b)
	{
		return a = a + b;
	}

	void tataAsync(int b, function<void(int&)> return_function = nullptr, function<void(std::exception&)> exception_function = nullptr)
	{
		execAsynchronously<int, exception, toto>(return_function, exception_function, shared_from_this(), &toto::tata, b);
	}

	int titi(int b)
	{
		this_thread::sleep_for(chrono::milliseconds(150));
		return a = a - b;
	}

	void tutu()
	{
		std::cout << a << " "
				  << "OK !!!" << std::endl;
	}

	void tutuCopyAsync()
	{
		shared_ptr<toto> tmp = make_shared<toto>(*this);
		tmp->a				 = 9;
		tmp->execVoidAsynchronously<exception, toto>(nullptr, nullptr, tmp, &toto::tutu);
	}
};


TEST_CASE("Lockable", "[AsynchronousExecution]")
{
	shared_ptr<toto> t = make_shared<toto>(6);

	t->execAsynchronously<int, exception, toto>(
			[](int& i) { cout << i << endl; }, [](auto& ex) { cout << ex.what() << endl; }, nullptr, &toto::titi, 20);

	this_thread::sleep_for(chrono::milliseconds(50));

	t->tutuCopyAsync();

	t->tataAsync(7, [](auto& i) { cout << i << endl; });

	execVoidFunction(t->getVoidFunction<toto>(&toto::tutu));
}
