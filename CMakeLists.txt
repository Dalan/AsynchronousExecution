cmake_minimum_required(VERSION 3.11)

project(asynchronous_execution CXX)

set (CMAKE_CXX_STANDARD 17)

set (CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")

option(ASYNC_EXEC_BUILD_TESTS "Build test programs" OFF)
option(ASYNC_EXEC_CODE_COVERAGE "Build with code coverage enable" OFF)

find_package(
	Threads
	REQUIRED
)

file(
	GLOB
	source_files
	"src/*.cpp"
	"include/*.h"
)

add_library(
	asynchronous_execution
	STATIC
	${source_files}
)

target_include_directories(
	asynchronous_execution
	PUBLIC
	"include"
)

target_link_libraries(
	asynchronous_execution
	PUBLIC
	${CMAKE_THREAD_LIBS_INIT}
)

#
# Code coverage
#
IF(ASYNC_EXEC_CODE_COVERAGE)
	SET(CMAKE_BUILD_TYPE Coverage)
ENDIF()

#
# Tests
#
IF(ASYNC_EXEC_BUILD_TESTS)
	enable_testing()
	add_subdirectory("test")
ENDIF()
